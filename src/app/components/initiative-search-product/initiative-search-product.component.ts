import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Initiative, Product } from 'src/app/models/product.model';
import { ProductMutationService } from 'src/app/services/productmutation.service';

@Component({
  selector: 'app-initiative-search-product',
  templateUrl: './initiative-search-product.component.html',
  styleUrls: ['./initiative-search-product.component.css']
})
export class InitiativeSearchProductComponent implements OnInit {
  initiatives: Initiative[] = [];


  products: Product[] = [];

  searInitiativeProductForm: FormGroup;
  selectedProduct!: Product;

  constructor(
    public fb: FormBuilder,
    private productMutationService: ProductMutationService,
    private route: ActivatedRoute
  ) {
    this.searInitiativeProductForm = this.fb.group({
      product: [''],

    });
  }

  async ngOnInit(): Promise<void> {
    await this.fetchProducts();
    this.selectedProduct  = this.products[0];
    await this.fetchInitiativesByProduct(this.selectedProduct.id);

  }

  async fetchProducts(){
    const result = await this.productMutationService.fetchOnlyProducts();

    this.products = result.products;

  }
  async fetchInitiativesByProduct(productId: string){
    const result = await this.productMutationService.fetchInitiativesByProduct(productId);
    this.initiatives = result.initiativesByProduct;

  }
  productChanged(product: Product){
    console.log(product);
    this.selectedProduct = product;
    this.fetchInitiativesByProduct(this.selectedProduct.id);
  }


}
