import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Owner, OwnerInput } from 'src/app/models/product.model';
import { ProductMutationService } from 'src/app/services/productmutation.service';

@Component({
  selector: 'app-owner-home',
  templateUrl: './owner-home.component.html',
  styleUrls: ['./owner-home.component.css']
})
export class OwnerHomeComponent implements OnInit{

  owners: Owner[] = [];


  addOwnerForm: FormGroup;

  constructor(public fb: FormBuilder, private productMutationService: ProductMutationService){
    this.addOwnerForm = this.fb.group({
      name: [''],
      status: [''],
    });
  }

  async ngOnInit(): Promise<void> {
    this.fetchOwners();
  }

  async fetchOwners(){
    const result = await this.productMutationService.fetchAllOwners();
    this.owners = result.owners;

  }

   addOwner(){
    const owner: OwnerInput = {
      name: this.addOwnerForm.get('name')?.value,
      status: this.addOwnerForm.get('status')?.value
    };
    const result =  this.productMutationService.saveOwner(owner);
    result.subscribe(({ data }) => {
      this.fetchOwners();


    },
    error => {
      console.log('there was an error sending the query', error)
    });

  }
}
