import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Goal, Product, ProductInput } from 'src/app/models/product.model';
import { ProductMutationService } from 'src/app/services/productmutation.service';

@Component({
  selector: 'app-product-home',
  templateUrl: './product-home.component.html',
  styleUrls: ['./product-home.component.css']
})
export class ProductHomeComponent implements OnInit{

  products: Product[] = [];


  addProductForm: FormGroup;

  constructor(public fb: FormBuilder, private productMutationService: ProductMutationService){
    this.addProductForm = this.fb.group({
      title: [''],
      description: [''],
    });
  }

  async ngOnInit(): Promise<void> {
    this.fetchProducts();
  }

  async fetchProducts(){
    const result = await this.productMutationService.fetchAllProducts();
    this.products = result.products;

  }

   addProduct(){
    const product: ProductInput = {
      title: this.addProductForm.get('title')?.value,
      description: this.addProductForm.get('description')?.value
    };
    const result =  this.productMutationService.saveProduct(product);
    result.subscribe(({ data }) => {
      this.fetchProducts();


    },
    error => {
      console.log('there was an error sending the query', error)
    });

  }



}
