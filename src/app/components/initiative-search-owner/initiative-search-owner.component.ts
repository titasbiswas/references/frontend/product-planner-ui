import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Initiative, Owner, Product } from 'src/app/models/product.model';
import { ProductMutationService } from 'src/app/services/productmutation.service';

@Component({
  selector: 'app-initiative-search-owner',
  templateUrl: './initiative-search-owner.component.html',
  styleUrls: ['./initiative-search-owner.component.css']
})
export class InitiativeSearchOwnerComponent implements OnInit {
  initiatives: Initiative[] = [];


  owners: Owner[] = [];

  searInitiativOwnerForm: FormGroup;
  selectedOwner!: Owner;

  constructor(
    public fb: FormBuilder,
    private productMutationService: ProductMutationService,
    private route: ActivatedRoute
  ) {
    this.searInitiativOwnerForm = this.fb.group({
      owner: [''],
    });
  }

  async ngOnInit(): Promise<void> {
    await this.fetchOwners();
    this.selectedOwner  = this.owners[0];
    await this.fetchInitiativesByOwner(this.selectedOwner.id);

  }

  async fetchOwners(){
    const result = await this.productMutationService.fetchAllOwners();
    this.owners = result.owners;

  }
  async fetchInitiativesByOwner(ownerId: string){
    const result = await this.productMutationService.fetchInitiativesByOwner(ownerId);
    this.initiatives = result.initiativesByOwner;

  }
  ownerChanged(owner: Owner){
    this.selectedOwner = owner;
    this.fetchInitiativesByOwner(this.selectedOwner.id);
  }


}
