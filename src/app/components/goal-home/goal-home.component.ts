import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Goal, GoalInput } from 'src/app/models/product.model';
import { ProductMutationService } from 'src/app/services/productmutation.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-goal-home',
  templateUrl: './goal-home.component.html',
  styleUrls: ['./goal-home.component.css'],
})
export class GoalHomeComponent implements OnInit {
  goals: Goal[] = [];
  productId: string = '';
  productName: string = '';

  addGoalForm: FormGroup;

  constructor(
    public fb: FormBuilder,
    private productMutationService: ProductMutationService,
    private route: ActivatedRoute
  ) {
    this.addGoalForm = this.fb.group({
      title: [''],
      description: [''],
    });
  }

  async ngOnInit(): Promise<void> {


    this.route.paramMap.subscribe((params) => {
        this.productId = params.get('productId')!;
        this.fetchGoals(this.productId);
      });


  }

  async fetchGoals(productId: string) {
    const result = await this.productMutationService.fetchGoalsForProduct(
      productId
    );
    this.goals = result.productById.goals;
    this.productName = result.productById.title;
  }

  addGoal() {
    const goal: GoalInput = {
      title: this.addGoalForm.get('title')?.value,
      description: this.addGoalForm.get('description')?.value,
    };
    console.log(goal);
    const result = this.productMutationService.addGoalsToProduct(
      goal,
      this.productId
    );
    result.subscribe(
      ({ data }) => {
        this.fetchGoals(this.productId);
      },
      (error) => {
        console.log('there was an error sending the query', error);
      }
    );
  }


}
