import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Initiative, InitiativeInput, Owner } from 'src/app/models/product.model';
import { ProductMutationService } from 'src/app/services/productmutation.service';

import moment, * as _moment from 'moment';
import { Observable, startWith,map } from 'rxjs';



@Component({
  selector: 'app-initiative-home',
  templateUrl: './initiative-home.component.html',
  styleUrls: ['./initiative-home.component.css']
})
export class InitiativeHomeComponent implements OnInit {
  initiatives: Initiative[] = [];
  goalId: string ='';
  goalName: string='';
  productName: string = '';

  owners: Owner[] = [];
  filteredOwners: Observable<Owner[]> | undefined;

  addInitiativeForm: FormGroup;

  constructor(
    public fb: FormBuilder,
    private productMutationService: ProductMutationService,
    private route: ActivatedRoute
  ) {
    this.addInitiativeForm = this.fb.group({
      title: [''],
      successCriteria: [''],
      priority: [''],
      startDate: [''],
      endDate: [''],
      owner: [''],
    });
  }

  async ngOnInit(): Promise<void> {

    this.route.paramMap.subscribe((params) => {
        this.goalId = params.get('goalId')!;
        this.fetchGoals(this.goalId);
      });

    await this.fetchOwners();
    // this.filteredOwners = this.addInitiativeForm.get('owner')?.valueChanges
    // .pipe(
    //   startWith(this.owners[0]),
    //   map((value: Owner) => this._filter(value || this.owners[0])),
    // );
  }
  private _filter(ownew: Owner): Owner[] {
    const filterValue = ownew.name.toLowerCase();

    return this.owners.filter(owner => owner.name.toLowerCase().includes(filterValue));
  }

  async fetchOwners(){
    const result = await this.productMutationService.fetchAllOwners();
    this.owners = result.owners;

  }

  async fetchGoals(goalId: string) {
    const result = await this.productMutationService.fetchInitiativesByGoal(
      goalId
    );
    this.initiatives = result.goalById.initiatives;
    this.goalName = result.goalById.title;
  }

  addInitiativeToGoal() {
    const startDate :moment.Moment= this.addInitiativeForm.get('startDate')?.value;
    const endDate = this.addInitiativeForm.get('endDate')?.value

    const initiative: InitiativeInput = {
      title: this.addInitiativeForm.get('title')?.value,
      successCriteria: this.addInitiativeForm.get('successCriteria')?.value,
      startDate: startDate.format('yyyy-MM-DD'),
      endDate: endDate.format('yyyy-MM-DD'),
      owner: this.addInitiativeForm.get('owner')?.value.id,
      priority: this.addInitiativeForm.get('priority')?.value,


    };
    const result = this.productMutationService.addInitiativeToGoal(
      initiative,
      this.goalId
    );
    result.subscribe(
      ({ data }) => {
        this.fetchGoals(this.goalId);
      },
      (error) => {
        console.log('there was an error sending the query', error);
      }
    );

    this.addInitiativeForm.reset();
  }


}
