import { gql } from 'apollo-angular';

const GET_PRODUCTS = gql`
  query {
    products {
      id
      title
      description
      goals {
        id
        title
        description
      }
    }
  }
`;

const ADD_PRODUCT = gql`
  mutation CreateProduct($product: ProductInput!) {
    createProduct(product: $product) {
      id
      title
      description
    }
  }
`;

const GET_OWNERS = gql`
  query {
    owners {
      id
      name
      status
    }
  }
`;

const ADD_OWNER = gql`
  mutation CreateOwner($ownerInput: OwnerInput!) {
    createOwner(ownerInput: $ownerInput) {
      id
      name
      status
    }
  }
`;

const DELETE_TODO = gql`
  mutation deleteTodo($id: Int!) {
    deleteTodo(id: $id) {
      id
    }
  }
`;
const GET_GOALS_BY_PRODUCT_ID = gql`
  query ProductById($productId: String!) {
    productById(productId: $productId) {
      id
      title
      goals {
        title
        description
        id
        initiatives {
          id
          title
          successCriteria
          startDate
          endDate
          status
          jiraLink
          dependencies {
            id
            title
          }
        }
      }
    }
  }
`;
const ADD_NEW_GOAL_TO_PRODUCT = gql`
  mutation AddNewGoalToProduct($goal: GoalInput!, $productId: String!) {
    addNewGoalToProduct(goal: $goal, productId: $productId) {
      id
      title
      description
    }
  }
`;

const GET_INITIATIVES_BY_GOAL_ID = gql`
  query GoalById($goalId: String!) {
    goalById(goalId: $goalId) {
      id
      title
      initiatives {
        id
        title
        successCriteria
        startDate
        endDate
        priority
        status
        jiraLink
        owner {
          id
          name
          status
        }
        dependencies {
          id
          title
          successCriteria
          owner {
            id
            name
            status
          }
        }
      }
    }
  }
`;

const GET_INITIATIVES_BY_PRODUCT_ID = gql`
  query InitiativesByProduct($product: String!) {
    initiativesByProduct(product: $product) {
      id
      title
      successCriteria
      startDate
      endDate
      priority
      status
      jiraLink
      owner {
        id
        name
        status
      }
      dependencies {
        id
        title
        successCriteria
        owner {
          id
          name
          status
        }
      }
    }
  }
`;

const GET_ONLY_PRODUCTS = gql`
  query {
    products {
      id
      title
      description
    }
  }
`;
const GET_INITIATIVES_BY_OWNER_ID = gql`
  query InitiativesByOwner($owner: String!) {
    initiativesByOwner(owner: $owner) {
      id
      title
      successCriteria
      startDate
      endDate
      priority
      status
      jiraLink
      owner {
        id
        name
        status
      }
      dependencies {
        id
        title
        successCriteria
        owner {
          id
          name
          status
        }
      }
    }
  }
`;

const ADD_NEW_INITIATIVE_TO_GOAL = gql`
  mutation AddNewInitiativeToGoal(
    $initiative: InitiativeInput!
    $goalId: String!
  ) {
    addNewInitiativeToGoal(initiative: $initiative, goalId: $goalId) {
      id
      title
      description
    }
  }
`;

export {
  GET_PRODUCTS,
  ADD_PRODUCT,
  DELETE_TODO,
  GET_GOALS_BY_PRODUCT_ID,
  ADD_NEW_GOAL_TO_PRODUCT,
  GET_OWNERS,
  ADD_OWNER,
  ADD_NEW_INITIATIVE_TO_GOAL,
  GET_INITIATIVES_BY_GOAL_ID,
  GET_INITIATIVES_BY_OWNER_ID,
  GET_INITIATIVES_BY_PRODUCT_ID,
  GET_ONLY_PRODUCTS,
};
