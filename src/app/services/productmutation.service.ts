import { query } from '@angular/animations';
import { Injectable } from '@angular/core';
import { Apollo, MutationResult } from 'apollo-angular';
import { Observable } from 'rxjs';
import { GoalInput, InitiativeInput, OwnerInput, Product, ProductInput } from '../models/product.model';
import { ADD_NEW_GOAL_TO_PRODUCT, ADD_NEW_INITIATIVE_TO_GOAL, ADD_OWNER, ADD_PRODUCT, GET_GOALS_BY_PRODUCT_ID, GET_INITIATIVES_BY_GOAL_ID, GET_INITIATIVES_BY_OWNER_ID, GET_INITIATIVES_BY_PRODUCT_ID, GET_ONLY_PRODUCTS, GET_OWNERS, GET_PRODUCTS } from './graphql.queries';

@Injectable({
  providedIn: 'root'
})
export class ProductMutationService {



  constructor(private apollo: Apollo) { }
  products: Product[] = [];
  loading = true
  error: any


  async fetchAllProducts(): Promise<any>{
    const result = await this.apollo
    .watchQuery({
      query: GET_PRODUCTS
    }).refetch();

    return result.data;
  }

   saveProduct(productInput: ProductInput){
    const result = this.apollo
    .mutate({
      mutation: ADD_PRODUCT,
      variables:{
        product:productInput
      }
    })
    return result;
  }

  async fetchAllOwners(): Promise<any>{
    const result = await this.apollo
    .watchQuery({
      query: GET_OWNERS
    }).refetch();

    return result.data;
  }

   saveOwner(ownerInput: OwnerInput){
    const result = this.apollo
    .mutate({
      mutation: ADD_OWNER,
      variables:{
        ownerInput:ownerInput
      }
    })
    return result;
  }

  addGoalsToProduct(goalInput: GoalInput, productId: string) {
    const result = this.apollo
    .mutate({
      mutation: ADD_NEW_GOAL_TO_PRODUCT,
      variables:{
        goal:goalInput,
        productId: productId
      }
    })
    return result;
  }

  async fetchGoalsForProduct(productId: string):Promise<any> {
    const result = await this.apollo
    .watchQuery({
      query: GET_GOALS_BY_PRODUCT_ID,
      variables:{
        productId: productId
      }
    }).refetch();
    return result.data;
  }


  addInitiativeToGoal(initiative: InitiativeInput, goalId: string) {
    const result = this.apollo
    .mutate({
      mutation: ADD_NEW_INITIATIVE_TO_GOAL,
      variables:{
        initiative:initiative,
        goalId: goalId
      }
    })
    return result;
  }

  async fetchInitiativesByGoal(goalId: string):Promise<any> {
    const result = await this.apollo
    .watchQuery({
      query: GET_INITIATIVES_BY_GOAL_ID,
      variables:{
        goalId: goalId
      }
    }).refetch();
    return result.data;
  }

  async fetchOnlyProducts(): Promise<any>{
    const result = await this.apollo
    .watchQuery({
      query: GET_ONLY_PRODUCTS
    }).refetch();

    return result.data;
  }

  async fetchInitiativesByProduct(product: string):Promise<any> {
    const result = await this.apollo
    .watchQuery({
      query: GET_INITIATIVES_BY_PRODUCT_ID,
      variables:{
        product: product
      }
    }).refetch();
    return result.data;
  }

  async fetchInitiativesByOwner(owner: string):Promise<any> {
    const result = await this.apollo
    .watchQuery({
      query: GET_INITIATIVES_BY_OWNER_ID,
      variables:{
        owner: owner
      }
    }).refetch();
    return result.data;
  }


}
