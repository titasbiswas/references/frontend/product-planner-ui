type Product = {
  id: string;
  title: string;
  description: string;
  goals: Goal[];
}

type Goal = {
  id: string;
  title: string;
  description: string;
  initiatives: Initiative[];
}

type Initiative = {
  id: string;
  title: string;
  successCriteria: string;
  priority: number;
  startDate: string;
  endDate: string;
  status: string;
  dependencies: Initiative[];
  owner: Owner;
  jiraLink: string;
}

type Owner = {
  id: string;
  name: string;
  status: string;
}

type ProductInput = {
  title?: string;
  description?: string;
  goals?: Goal[];
}

type GoalInput = {
  title: string;
  description: string;
  initiatives?: Initiative[];
}

type InitiativeInput = {
  title: string;
  successCriteria: string;
  priority: number;
  startDate: string;
  endDate: string;
  owner: String;
}

type OwnerInput = {
  name: string;
  status: string;
}

export {Owner, Product, Goal, Initiative, GoalInput, InitiativeInput, OwnerInput, ProductInput}
