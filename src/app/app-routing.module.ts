import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GoalHomeComponent } from './components/goal-home/goal-home.component';
import { InitiativeDetailComponent } from './components/initiative-detail/initiative-detail.component';
import { InitiativeHomeComponent } from './components/initiative-home/initiative-home.component';
import { InitiativeSearchOwnerComponent } from './components/initiative-search-owner/initiative-search-owner.component';
import { InitiativeSearchProductComponent } from './components/initiative-search-product/initiative-search-product.component';
import { OwnerHomeComponent } from './components/owner-home/owner-home.component';
import { ProductHomeComponent } from './components/product-home/product-home.component';

const routes: Routes = [
  {
    path: '',
    component: ProductHomeComponent
  },
  {
    path: 'product-home',
    component: ProductHomeComponent
  },
  {
    path: 'goal-home/:productId',
    component: GoalHomeComponent
  },
  {
    path: 'initiative-home/:goalId',
    component: InitiativeHomeComponent
  },
  {
    path: 'owner-home',
    component: OwnerHomeComponent
  },
  {
    path: 'intv-search-product',
    component: InitiativeSearchProductComponent
  },
  {
    path: 'intv-search-owner',
    component: InitiativeSearchOwnerComponent
  },
  {
    path: 'intv-detail',
    component: InitiativeDetailComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
