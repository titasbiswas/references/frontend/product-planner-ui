import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LayoutComponent } from './shared/layout/layout.component';
import { ProductHomeComponent } from './components/product-home/product-home.component';
import { CustomMaterialModule } from './shared/custom-material/custom-material.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { GraphQLModule } from './graphql.module';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { GoalHomeComponent } from './components/goal-home/goal-home.component';
import { OwnerHomeComponent } from './components/owner-home/owner-home.component';
import { InitiativeHomeComponent } from './components/initiative-home/initiative-home.component';
import { InitiativeSearchProductComponent } from './components/initiative-search-product/initiative-search-product.component';
import { InitiativeSearchOwnerComponent } from './components/initiative-search-owner/initiative-search-owner.component';
import { InitiativeDetailComponent } from './components/initiative-detail/initiative-detail.component';

@NgModule({
  declarations: [
    AppComponent,
    LayoutComponent,
    ProductHomeComponent,
    GoalHomeComponent,
    OwnerHomeComponent,
    InitiativeHomeComponent,
    InitiativeSearchProductComponent,
    InitiativeSearchOwnerComponent,
    InitiativeDetailComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    CustomMaterialModule,
    FlexLayoutModule,
    GraphQLModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
